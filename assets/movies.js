const api='https://api.themoviedb.org/3/';
const api_key = 'api_key=a3ad03e1fba8c7169fbc18ac03d3216d';

async function loadPageDefault(){
    const reqDe = `${api}movie/top_rated?${api_key}&page=1`;

    loading();
    const respDe = await fetch(reqDe);;
    const rsDe = await respDe.json();

    fillMovie(rsDe);
}

async function evtsubmit(e){
    e.preventDefault();

    const strSearch = $('form input').val();
    const reqStr = `${api}search/multi?${api_key}&query=${strSearch}`;
    loading();

    const response = await fetch(reqStr);
    const rs = await response.json();

    fillMovie(rs);
}



function fillMovie(ms)
{
    $('#main').empty();
    for(const m of ms.results){
        if($(m).attr('known_for'))
        {
            for(const t of $(m.known_for))
            {
                if($(t).attr('original_title'))
                {
                    $('#main').append(`
                        <div class="col-md-4 py-1">
                            <div class="card shadow h-100" onclick="loadDetail('${t.id}');">
                            <img src="https://image.tmdb.org/t/p/w500${t.poster_path}" class="card-img-top" alt="${t.title}">
                                <div class="card-body">
                                    <h4 class="card-title">${t.title}</h4>
                                    <h5 class="my-4">Rate:
                                        <small>${t.vote_average}(${t.vote_count})</small>
                                    </h5>
                                    <h5 class="my-4">Description:<br/>
                                        <small>${t.overview}</small>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    `);
                }
                else
                {
                    $('#main').append(`
                        <div class="col-md-4 py-1">
                            <div class="card shadow h-100" onclick="loadDetail('${t.id}');">
                            <img src="https://image.tmdb.org/t/p/w500${t.poster_path}" class="card-img-top" alt="${t.name}">
                                <div class="card-body">
                                    <h4 class="card-title">${t.name}</h4>
                                    <h5 class="my-4">Rate:
                                        <small>${t.vote_average}(${t.vote_count})</small>
                                    </h5>
                                    <h5 class="my-4">Description:<br/>
                                        <small>${t.overview}</small>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    `);
                }
            }
        }
        else
        {
            if($(m).attr('original_title'))
            {
                $('#main').append(`
                    <div class="col-md-4 py-1">
                        <div class="card shadow h-100" onclick="loadDetail('${m.id}');">
                        <img src="https://image.tmdb.org/t/p/w500${m.poster_path}" class="card-img-top" alt="${m.title}">
                            <div class="card-body">
                                <h4 class="card-title">${m.title}</h4>
                                <h5 class="my-4">Rate:
                                    <small>${m.vote_average}(${m.vote_count})</small>
                                </h5>
                                <h5 class="my-4">Description:<br/>
                                    <small>${m.overview}</small>
                                </h5>
                            </div>
                        </div>
                    </div>
                `);
            }
            else
            {
                $('#main').append(`
                    <div class="col-md-4 py-1">
                        <div class="card shadow h-100" onclick="loadDetail('${m.id}');">
                        <img src="https://image.tmdb.org/t/p/w500${m.poster_path}" class="card-img-top" alt="${m.name}">
                            <div class="card-body">
                                <h4 class="card-title">${m.name}</h4>
                                <h5 class="my-4">Rate:
                                    <small>${m.vote_average}(${m.vote_count})</small>
                                </h5>
                                <h5 class="my-4">Description:<br/>
                                    <small>${m.overview}</small>
                                </h5>
                            </div>
                        </div>
                    </div>
                `);
            }
        }
    }
}

function loading(){
    $('#main').empty();
    $('#main').append(`
        <div class="d-flex justify-content-center">
            <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    `)
}

async function loadDetail(id)
{
    const reqStr = `${api}movie/${id}?${api_key}`;
    const reqCC = `${api}movie/${id}/credits?${api_key}`;
    const reqRv = `${api}movie/${id}/reviews?${api_key}`;

    loading();

    const response = await fetch(reqStr);
    const movie = await response.json();
    const respCC = await fetch(reqCC);
    const castcrew = await respCC.json();
    const respRv = await fetch(reqRv);
    const review = await respRv.json();

    fillMovieDetail(movie, castcrew, review);
}

function fillMovieDetail(m,cs, rv){
    $('#main').empty();
    $('#main').append(`
        <div class="container">
            <div class="row shadow">
                <div class="col-md-4">
                    <img src="https://image.tmdb.org/t/p/w500${m.poster_path}" class="card-img-top" alt="${m.title}">
                </div>
                <div class="col-md-8">
                    <h3 class="my-4">${m.title}
                        <small id="year">(${m.release_date})</small>
                    </h3>
                    <h4 class="my-4">Rate:
                        <small>${m.vote_average}(${m.vote_count})</small>
                    </h4>
                    <h4 class="my-4">Overview: <br/>
                        <small>${m.overview}</small>
                    </h4>
                    <h4 class="my-4">Tagline:
                        <small>${m.tagline}</small>
                    </h4>
                </div>
            </div>
            
            <div class="row shadow"> 
                <div class="col-md-7 shadow">
                    <h4 class="my-4">Cast:
                        <div class="container shadow">
                            <div id="cast" class="row py-1" >
                            </div>
                        </div>
                    </h4>
                    <h4 class="my-4">Director:
                        <div class="container shadow">
                            <div id="director" class="row py-1" >
                            </div>
                        </div>
                    </h4>
                    <h4 class="my-4">Crew:
                        <div class="container shadow">
                            <div id="crew" class="row py-1" >
                            </div>
                        </div>
                    </h4>
                    <h4 class="my-4">Review:
                        <div class="container shadow">
                            <div id="review" class="row py-1" >
                            </div>
                        </div>
                    </h4>
                    <h4 class="my-4">Production companies:
                        <div class="container shadow">
                            <div id="company" class="row py-1" >
                            </div>
                        </div>
                    </h4>
                </div>
               
                <div class="col-md-5 shadow">
                    <p>
                        <h4 class="my-4" style="word-wrap: break-word;">Homepage: 
                            <i><small>${m.homepage}</small></i>
                        </h4>
                        <h4 class="my-4">Language:
                            <i><small>${m.original_language}</small></i>
                        </h4>
                        <h4 class="my-4">Popularity:
                            <small>${m.popularity}</small>
                        </h4>
                        <h4 class="my-4">Genres:
                            <p id="genres"></p>
                        </h4>
                        <h4 class="my-4">production_countries:
                            <p id="country"></p>
                        </h4>
                        <h4 class="my-4">Release date:
                            <small>${m.release_date}</small>
                        </h4>
                        <h4 class="my-4">Revenue:
                            <small>${m.revenue}</small>
                        </h4>
                        <h4 class="my-4">Runtime:
                            <small>${m.runtime}m</small>
                        </h4>
                    </p>
                </div>
            </div>
        </div>
    `)
    $('#company').empty();
    for(const mv of $(m.production_companies))
    {
        $('#company').append(`
            <div class="col-md-4 py-1">
                <div class="card shadow h-100">
                    <img src="https://image.tmdb.org/t/p/w500${mv.logo_path}" class="card-img-top" alt="${mv.name}">
                    <div class="card-body">
                        <h4 class="card-title">${mv.name}</h4>
                    </div>
                </div>
            </div>
        `)
    }
    $('#genres').empty();
    for(const gr of $(m.genres))
    {
        $('#genres').append(`
            <small>${gr.name}<br></small>
        `)
    }
    $('#country').empty();
    for(const ct of $(m.production_countries))
    {
        $('#country').append(`
            <small>${ct.name}<br></small>
        `)
    }
    fillCastCrew(cs);
    fillReview(rv);
    document.getElementById("year").innerHTML = "(" + `${m.release_date}`.slice(0, 4)+ ")";
}

function fillCastCrew(ccs){
    var i=0;
    $('#cast').empty();
    for(const cs of $(ccs.cast))
    {
        if(i===5)
        {
            i=0;
            break;
        }
        $('#cast').append(`
            <div class="col-md-4 py-1">
                <div class="card shadow h-100" onclick="loadPerson('${cs.id}');">
                    <img src="https://image.tmdb.org/t/p/w500${cs.profile_path}" class="card-img-top" alt="${cs.name}">
                    <div class="card-body">
                    <h5 class="my-4">${cs.name}<br/>
                        <small>(${cs.character})</small>
                    </h5>
                    </div>
                </div>
            </div>
        `)
        i=i+1;
    }
    $('#director').empty();
    for(const drt of $(ccs.crew))
    {
        if(`${drt.job}` === 'Director')
        {
            $('#director').append(`
                <div class="col-md-4 py-1">
                    <div class="card shadow h-100">
                        <img src="https://image.tmdb.org/t/p/w500${drt.profile_path}" class="card-img-top" alt="${drt.name}">
                        <div class="card-body">
                        <h5 class="my-4">${drt.name}<br/>
                            <small>(${drt.job})</small>
                        </h5>
                        </div>
                    </div>
                </div>
            `)
            break;
        }
    }
    $('#crew').empty();
    for(const crs of $(ccs.crew))
    {
        if(i===5)
        {
            break;
        }
        if(`${crs.job}`!== 'Director')
        {
            $('#crew').append(`
                <div class="col-md-4 py-1">
                    <div class="card shadow h-100">
                        <img src="https://image.tmdb.org/t/p/w500${crs.profile_path}" class="card-img-top" alt="${crs.name}">
                        <div class="card-body">
                        <h5 class="my-4">${crs.name}<br/>
                            <small>(${crs.job})</small>
                        </h5>
                        </div>
                    </div>
                </div>
            `)
            i=i+1;
        }
    }
}

function fillReview(rvs){
    $('#review').empty();
    var i=0;
    for(const rv of $(rvs.results))
    {
        if(i===5)
        {
            break;
        }
        $('#review').append(`
            <div class="card-body">
                <h5 class="my-4">${rv.author}
                    <small>(${rv.url})<br/>
                    ${rv.content}</small>
                </h5>
            </div>
        `)
        i=i+1;
    }
}

async function loadPerson(id)
{
    const reqPs = `${api}person/${id}?${api_key}`;
    const reqMvPs = `${api}person/${id}/movie_credits?${api_key}`;

    loading();

    const respPs = await fetch(reqPs);
    const person = await respPs.json();
    const respMvPs = await fetch(reqMvPs);
    const personMv = await respMvPs.json();

    fillPersonDetail(person, personMv);
}

function fillPersonDetail(ps,psmv){
    $('#main').empty();
    $('#main').append(`
        <div class="container">
            <div class="row shadow">
                <div class="col-md-4">
                    <img src="https://image.tmdb.org/t/p/w500${ps.profile_path}" class="card-img-top" alt="${ps.name}">
                </div>
                <div class="col-md-8">
                    <h3 class="my-4">${ps.name}</h3>
                    <h4 class="my-4">Birthday:
                        <small>${ps.birthday}</small>
                    </h4>
                    <h4 class="my-4">Place of birth: 
                        <small>${ps.place_of_birth}</small>
                    </h4>
                    <h4 class="my-4">Known for department:
                        <small>${ps.known_for_department}</small>
                    </h4>
                    <h4 class="my-4">Biography: <br/>
                        <small>${ps.biography}</small>
                    </h4>
                </div>
            </div>

            <div id="list_movie" class="row py-1" >
                
            </div>
        </div>
    `);
    fillMoviePerson(psmv);
}

function fillMoviePerson(mvps)
{
    $('#list_movie').empty();
    for(const lmv of mvps.cast){
        $('#list_movie').append(`
            <div class="col-md-4 py-1">
                <div class="card shadow h-100" onclick="loadDetail('${lmv.id}');">
                <img src="https://image.tmdb.org/t/p/w500${lmv.poster_path}" class="card-img-top" alt="${lmv.title}">
                    <div class="card-body">
                        <h4 class="card-title">${lmv.title}</h4>
                        <h5 class="my-4">Rate:
                            <small>${lmv.vote_average}(${lmv.vote_count})</small>
                        </h5>
                        <h5 class="my-4">Description:<br/>
                            <small>${lmv.overview}</small>
                        </h5>
                    </div>
                </div>
            </div>
        `);
    }
}